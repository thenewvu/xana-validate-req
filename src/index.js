'use strict';

const validator       = require('xana-validator');
const ValidationError = require('xana-errors').ValidationError;

/**
 * Return a prev-middleware that validates coming requests.
 * @param {Array.<Array>} schema
 * @param {Object} [opts]
 * @returns {Function}
 */
function validateReq(schema, opts) {
  const validate = validator.create(schema, opts);
  return (req, next) => {
    validate(req, (errors) => {
      next(errors ? new ValidationError(errors) : null, req);
    });
  };
}

module.exports = validateReq;

